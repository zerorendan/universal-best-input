//
//  ViewController.swift
//  Universal
//
//  Created by Juan Calvo on 8/16/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialButtons_ButtonThemer

class ViewController: UIViewController {
    
    //MARK: - Attributes
    @IBOutlet var numericField: MDCTextField!
    var numericController:MDCTextInputControllerOutlined?
    
    @IBOutlet var emailField: MDCTextField!
    var emailController:MDCTextInputControllerOutlined?
    
    @IBOutlet var phoneField: MDCTextField!
    var phoneController:MDCTextInputControllerOutlined?
    
    @IBOutlet var validationButton: MDCButton!
    var buttonScheme = MDCButtonScheme()
    
    let maxNumericLength = 10
    let maxPhoneLength = 8
    let numericFieldTag = 0
    let phoneFieldTag = 1
    let emailFieldTag = 2
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        setupStyle()
    }
    
    //MARK: - UI Logic
    private func configure() {
        numericField.delegate = self
        numericField.tag = numericFieldTag
        phoneField.delegate = self
        phoneField.tag = phoneFieldTag
        emailField.delegate = self
        emailField.tag = emailFieldTag
    }
    
    private func setupStyle() {
        numericController = MDCTextInputControllerOutlined(textInput: numericField)
        numericController?.placeholderText = "Numeros"
        numericController?.characterCountMax = UInt(maxNumericLength)
        numericController?.errorColor = .red
        
        emailController = MDCTextInputControllerOutlined(textInput: emailField)
        emailController?.placeholderText = "Email"
        emailController?.errorColor = .red
        
        phoneController = MDCTextInputControllerOutlined(textInput: phoneField)
        phoneController?.placeholderText = "Telefono"
        phoneController?.characterCountMax = UInt(maxPhoneLength)
        phoneController?.errorColor = .red
        
        MDCOutlinedButtonThemer.applyScheme(buttonScheme, to: validationButton)
    }
    
    @IBAction func validate(_ sender: MDCButton) {
        
    }
}

//MARK: - UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case numericFieldTag:
            let eval = Constans.Validation(text: string,
                                           fieldLenght: textField.text!.count + string.count,
                                           maxValue: maxNumericLength,
                                           regex: .number,
                                           errorMessage: "Solo numeros son permitidos")
            return validate(data: eval, controller: numericController!)
        case phoneFieldTag:
            let eval = Constans.Validation(text: string,
                                           fieldLenght: textField.text!.count + string.count,
                                           maxValue: maxPhoneLength,
                                           regex: .phone,
                                           errorMessage: "Solo telefonos son permitidos")
            return validate(data: eval, controller: phoneController!)
        case emailFieldTag:
            return true
            
        default:
            print("Ivalid option")
            return false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == emailFieldTag {
            if !(textField.text?.isEmptyString())!{
                emailController?.setErrorText((textField.text?.isValid(.email))! ? nil: "Solo emails validos son permitidos", errorAccessibilityValue: nil)
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if textField.tag == emailFieldTag {
            guard textField.text != nil else { return }
            if !(textField.text?.isEmptyString())!{
                emailController?.setErrorText((textField.text?.isValid(.email))! ? nil: "Solo emails validos son permitidos", errorAccessibilityValue: nil)
            }
        }
    }
    
    private func validate(data: Constans.Validation, controller:MDCTextInputControllerOutlined) -> Bool {
        let isCorrectLength = data.fieldLenght <= data.maxValue
        let isValidToRegex = data.text.isValid(data.regex)
        controller.setErrorText( isValidToRegex ? nil:data.errorMessage, errorAccessibilityValue: nil)
        return isCorrectLength && isValidToRegex
    }
}

