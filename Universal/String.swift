//
//  String.swift
//  Universal
//
//  Created by Juan Calvo on 8/16/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

extension String {
    /// Evaluate String agains regex
    ///
    /// - Parameter regexExp: regular expression
    /// - Returns: true if the expression matches the string
    func isValid(_ regexExp: Constans.Regex) -> Bool {
        let validator = NSPredicate(format: "SELF MATCHES %@", regexExp.rawValue)
        return validator.evaluate(with:self)
    }
    
    func isEmptyString() -> Bool {
        return self == "" || self.count == 0
    }
}
