//
//  Constans.swift
//  Universal
//
//  Created by Juan Calvo on 8/16/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

struct Constans {
    enum Regex: String {
        case phone = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$"
        case number = "^[0-9]*$"
        case email = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
    }
    
    struct Validation {
        var text: String
        var fieldLenght: Int
        var maxValue:Int
        var regex : Regex
        var errorMessage: String
    }
}
